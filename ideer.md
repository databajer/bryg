# Ideer til øl

## Grøn Prince

Røgøl med pebermynte. Den skal smage af våd frakke efter man har været ude i
rygeskuret en efterårsdag.


## Levistico

Varm IPA med løvstikke og en lille smule salt.


## Suppeterning

Varm IPA med alt det nice fra boullion.


## Vinterdæk

Søsterøl til Sommerdæk, men varmere i smagen. Ikke helt så meget en
tørstslukker. Mere krydrede, granagtige humler.


## Hjuleøl

Et bud på en juleøl der ikke er røvsyg. Ægte mørk. Krydret som julen, med
kanel, appelsinskal, nelliker, osv.
